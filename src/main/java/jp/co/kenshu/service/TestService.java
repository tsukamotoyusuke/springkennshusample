package jp.co.kenshu.service;

import java.util.LinkedList;
import java.util.List;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.kenshu.dto.test.TestDto;
import jp.co.kenshu.entity.Test;
import jp.co.kenshu.mapper.TestMapper;

//@AutowiredでServiceに依存性を注入するには、TestServiceにも「@Service」というアノテーションをつけておくという規約があります。
@Service
public class TestService {

	@Autowired	//←によって、インターフェースを使って実クラスをインスタンス化しています。
	private TestMapper testMapper;

	public TestDto getTest(Integer id) {

		TestDto dto = new TestDto();
		Test entity = testMapper.getTest(id);

		/*
		 Serviceで行っていることはMapper(Dao)経由で取得したentityを
		BeanUtils.copyProperties(entity, dto);
		を使ってTestDtoに詰め替えてreturnしているだけです。
		testMapper.getTest(3);
		とした場合、最終的には
		select id, name from test where id = 3
		というSQLが発行されます。
		 */
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}

	//4章/機能の追加で追加
	//getTestAllでmapperから取得した結果をListに詰めています。この段階ではTestはentityです。
	public List<TestDto> getTestAll() {
		List<Test> testList = testMapper.getTestAll();
		List<TestDto> resultList = convertToDto(testList);
		return resultList;
	}

	private List<TestDto> convertToDto(List<Test> testList) {
		List<TestDto> resultList = new LinkedList<>();
		for (Test entity : testList) {
			TestDto dto = new TestDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	//4章 2. whereにオブジェクトを渡す

	//引数がdtoになっただけです。
	public TestDto getTestByDto(TestDto dto) {
		Test entity = testMapper.getTestByDto(dto);
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}

	//4章 3. Insertサンプル
	public int insertTest(String name) {
		int count = testMapper.insertTest(name);
		return count;
	}

	//4章 4. deleteのサンプル
	public int deleteTest(int id) {
		int count = testMapper.deleteTest(id);
		return count;
	}

	//4章 5. updateのサンプル
	public int updateTest(TestDto dto) {
		int count = testMapper.updateTest(dto);
		return count;
	}


	// //5章 transactionのテスト

	//たったアノテーションの記述一行でトランザクション処理が実現できました。
	@Transactional
	public void deleteAllAndInsert(TestDto dto) {

		int delCount = testMapper.deleteTest(dto.getId());
		Logger.getLogger(TestService.class).log(Level.INFO, "削除件数は" + delCount + "件です。");
		// ここのinsertは失敗する。deleteがrollBackされるかどうか。
		int insCount = testMapper.insertFailTest(null);
		Logger.getLogger(TestService.class).log(Level.INFO, "挿入件数は" + insCount + "件です。");
	}
}