package jp.co.kenshu.mapper;

import java.util.List;

import jp.co.kenshu.dto.test.TestDto;
import jp.co.kenshu.entity.Test;

public interface TestMapper {
	Test getTest(int id);

	//4章/機能の追加で追加
	List<Test> getTestAll();

	//4章 2. whereにオブジェクトを渡す
	Test getTestByDto(TestDto dto);

	//4章 3. Insertサンプル
	int insertTest(String name);

	//4章 4. deleteのサンプル
	int deleteTest(int id);

	//4章 5. updateのサンプル
	int updateTest(TestDto dto);

	// トランザクションテスト用の失敗メソッド
	int insertFailTest(Object object);
	/*
	TestMapper.javaの「getTest」が呼ばれた際に、TestMapper.xmlの
	<select id="getTest" resultMap="TestResult" parameterType="int">
		select id, name
		from test
		where id = #{id}
	</select>
	が呼ばれます。
	「id=“getTest”」に注目してください。TestMapper.javaのgetTestはxmlのこのIDと紐いて処理が行われる仕組みになってます。


TestMapper.xmlの<mapper namespace="jp.co.kenshu.mapper.TestMapper">でTestMapper.javaファイルと紐付けています。
	 */


}