package jp.co.kenshu.controller;

import java.util.List;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.test.TestDto;
import jp.co.kenshu.form.TestForm;
import jp.co.kenshu.service.TestService;


@Controller
public class TestController {

	/*DIとは「Dependency Injection」の略であり、「依存性の注入」と訳されます
	プログラム中でnew(インスタンス化)することなく、インターフェースを使って実クラスをインスタンス化します。*/
	@Autowired
	private TestService testService;

	@RequestMapping(value = "/test/{id}", method = RequestMethod.GET)
	//これの「{id}」はURLに変数としてidを指定しています。

	public String test(Model model, @PathVariable int id) {


		//↓でService経由で取得したDBセレクト情報をTestDtoという入れ物に詰めてあげて、
		//(「セレクト情報」とは、select id, name from test where id = 1;の取得結果です。)
		TestDto test = testService.getTest(id);

		model.addAttribute("message", "MyBatisのサンプルです");

		//結果を"test"というkey値でmodelにAttributeしてるだけです。
		model.addAttribute("test", test);
		return "test";
	}

	//4章/機能の追加で追加
	@RequestMapping(value = "/test/", method = RequestMethod.GET)
	public String testAll(Model model) {
		List<TestDto> tests = testService.getTestAll();
		model.addAttribute("message", "MyBatisの全件取得サンプルです");
		model.addAttribute("tests", tests);
		/*Controllerから渡された${tests}をtestAll.jspの<c:foreach>で回してます。
		回す際に、var="test"としているので、${test}がTestDtoになります。
		return "testAll";
		結果的に、${test.name}としているため、画面上には「test.getName()」した結果が表示されています。
		 */
		return "testAll";
	}


	//4章 2. whereにオブジェクトを渡す
	@RequestMapping(value = "/test/dto/{id}", method = RequestMethod.GET)

	//今回は/test/dto/{id}となっておりますので、/test/dto/2とかでアクセスするメソッドですね。{id}で渡されたパラメータを@PathVariable int idにより、変数idで受けています。
	public String testDto(Model model, @PathVariable int id) {

		TestDto dto = new TestDto();
		dto.setId(id);
		/*TestDto dto = new TestDto();
		dto.setId(id);
		で渡されたidをDtoに詰めてます。そして*/

		TestDto test = testService.getTestByDto(dto);
		//とし、serviceのgetTestByDtoメソッドにdtoを引数として渡しています。

		model.addAttribute("message", "MyBatisのサンプルです");
		model.addAttribute("test", test);
		return "test";
	}

	//4章 3. Insertサンプル
	/*GETが今回の入り口画面です。
	JSPでmodelAttributeの設定がform:formにしてあるので、modelに渡す必要があります。
	testInsert.jspにforwardしてます。*/

	@RequestMapping(value = "/test/insert/input/", method = RequestMethod.GET)
	public String testInsert(Model model) {
		TestForm form = new TestForm();
		model.addAttribute("testForm", form);
		model.addAttribute("message", "MyBatisのinsertサンプルです。");
		return "testInsert";
	}

	@RequestMapping(value = "/test/insert/input/", method = RequestMethod.POST)
	public String testInsert(@ModelAttribute TestForm form, Model model) {

		int count = testService.insertTest(form.getName());
		//Serviceを呼んで、insert成功した件数をlogに吐き出してるだけですね。
		Logger.getLogger(TestController.class).log(Level.INFO, "挿入件数は" + count + "件です。");

		//SpringMVCでredirectする場合は、「redirect:」と書いて、その後に遷移先を書くだけです。
		return "redirect:/test/";

	}

	//4章 4. deleteのサンプル
	@RequestMapping(value = "/test/delete/input/", method = RequestMethod.GET)
	public String testDelete(Model model) {
		TestForm form = new TestForm();
		model.addAttribute("testForm", form);
		model.addAttribute("message", "MyBatisのdeleteサンプルです。");
		return "testDelete";
	}

	@RequestMapping(value = "/test/delete/input/", method = RequestMethod.POST)
	public String testDelete(@ModelAttribute TestForm form, Model model) {
		int count = testService.deleteTest(form.getId());
		Logger.getLogger(TestController.class).log(Level.INFO, "削除件数は" + count + "件です。");
		return "redirect:/test/";
	}


	//4章 5. updateのサンプル
	@RequestMapping(value = "/test/update/input/{id}/", method = RequestMethod.GET)
	public String testUpdate(Model model, @PathVariable int id) {
		TestDto test = testService.getTest(id);
		model.addAttribute("message", "MyBatisのUpdateサンプルです");
		model.addAttribute("test", test);
		TestForm form = new TestForm();
		form.setId(test.getId());
		form.setName(test.getName());
		model.addAttribute("testForm", form);
		return "testUpdate";
	}

	@RequestMapping(value = "/test/update/input/{id}/", method = RequestMethod.POST)
	public String testUpdate(Model model, @ModelAttribute TestForm form) {
		TestDto dto = new TestDto();
		BeanUtils.copyProperties(form, dto);
		int count = testService.updateTest(dto);
		Logger.getLogger(TestController.class).log(Level.INFO, "更新件数は" + count + "件です。");
		return "redirect:/test/";
	}

	//5章 トランザクションのサンプル
	@RequestMapping(value = "/test/transaction/{id}", method = RequestMethod.GET)
	public String testTransaction(Model model, @PathVariable int id) {
		TestDto dto = new TestDto();
		dto.setId(id);
		testService.deleteAllAndInsert(dto);
		return "redirect:/test/";
	}
}